<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="contactWrapper">

			<!-- article -->
			<article id="post-404" class="txtFieldContainer">

				<h2 class="subHeader">Å nej, den sidan verkar inte finnas. Välj något i menyn eller <a href="<?php echo home_url(); ?>"><?php _e( 'gå tillbaka till start', 'html5blank' ); ?></a>. </h2>


			</article>
			<!-- /article -->

		</section>
		<!-- /section -->
	</main>


<?php get_footer(); ?>
