<?php get_header(); ?>

<main role="main" class="mainWrapper">
	<!-- section -->
	<?php
	$currentPost = get_the_ID();
	$heroImg = get_field('toppbild');
	$heroText = get_the_title();
	$productIntro = get_field('produktintroduktion');
	$productIngredients = get_field('ingredienser');
	$productNutrition = get_field('naringsvarden');
	$productAllergies = get_field('allergifakta');
	$productImg = get_field('produktbild')['url'];
	?>
<!-- top hero image section -->
	<!-- <section class="section span_12_of_12">
		<div class="topImgContainer" style="background-image:url('<?php echo $heroImg; ?>');"></div>
		<div class="section span_6_of_12 pageHeaderContainerProduct">
			<h1 class="pageHeader"><?php //echo $heroText; ?></h1>
		</div>
	</section> -->
	<section class="pageContentWrapper--no-img">


	<section class="section span_12_of_12 productWrapper">
		<section class="span_6_of_12 productImgContainer">
			<img src="<?php echo $productImg; ?>" class="productImg"/>
		</section>
		<section class="span_6_of_12 txtFieldContainer--singleProd"><!-- productTxtContainer. -->
			<h1 class="txtFieldHeader"><?php echo $heroText; ?></h1>
			<p class="productIntroTxt"><?php echo $productIntro; ?></p>
			<?php if(get_field('ingredienser')) : ?>
				<h5 class="productIntroSubHeader">Ingredienser</h5>
				<p class="productInformation"><?php echo $productIngredients; ?></p>
			<?php endif ?>

			<?php if(get_field('naringsvarden')) : ?>
				<h5 class="productIntroSubHeader">Näringsvärden</h5>
				<p class="productInformation"><?php echo $productNutrition; ?></p>
			<?php endif ?>

			<?php if(get_field('allergifakta')) : ?>
				<h5 class="productIntroSubHeader">Allergifakta</h5>
				<p class="productInformation"><?php echo $productAllergies; ?></p>
			<?php endif ?>




		</section><!-- productTxtContainer -->
	</section> <!-- /productWrapper -->

	<?php
		$productLoop = new WP_Query(array('post_type' => 'products') );
	?>

<div class="maxWidth">
	<section class="section span_12_of_12 allProducts">
    <div></div>
		<div></div>
		<?php
		$counter = 0;
		if($productLoop->have_posts() ) :
			while ($productLoop->have_posts() ) : $productLoop->the_post();
			  $productImg = get_field('produktbild')['url'];
				if(get_field('produktnamn')) {
					$productName = get_field('produktnamn');
				} else {
					$productName = get_the_title();
				}


				// $productName = get_field('produktnamn');
				if($currentPost !== $id) {?>
					<section class="otherProductContainer"><!-- otherProductContainer style="display:inline-block;width:150px;-->
						<a href="<?php the_permalink(); ?>"><img src="<?php echo $productImg; ?>" class="otherProductImg" /></a> <!-- otherProductImg -->
						<span class="otherProductTitle"><?php echo $productName; ?></span> <!-- otherProductTitle -->
					</section>
				<?php }

			?>

			<?php
			endwhile;
		endif;
			?>
</section>
	</section> <!-- allProducts -->
</div> <!-- maxWidth -->
</main>



<?php get_footer(); ?>
