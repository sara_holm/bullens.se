			<!-- footer -->
			<footer class="footer section span_12_of_12" role="contentinfo">


				<section class="footerContentContainer section span_12_of_12">
					<div class="footerLogo span_5_of_12 footerSection">
						<img src="<?php echo get_template_directory_uri(); ?>/img/logo/logo-bullens-white.png" alt="Logo" />
					</div>
					<div class="span_7_of_12 footerLinksContainer">
						<section class="span_4_of_12 footerSection">
							<h4 class="footerHeader">Site</h4>
							<?php bullens_footer_nav(); ?>
						</section>
						<section class="span_4_of_12 footerSection">
							<h4 class="footerHeader">Socialt</h4>
							<a href="https://www.facebook.com/bullens" class="footerLink footerBodyText">Facebook</a>
							<a href="https://www.instagram.com/bullenspilsnerkorv/" class="footerLink footerBodyText">Instagram</a>
						</section>
						<section class="span_4_of_12 footerSection">
							<h4 class="footerHeader">Kontakt</h4>
							<p class="footerBodyText">HKScan Sweden AB, Matforum</p>
							<p class="footerBodyText">tel 020-59 59 77</p>
							<p class="footerBodyText">
								<a class="footerLink" href="mailto:matforum@hkscan.com">matforum@hkscan.com</a>
							</p>
						</section>
					</div>

				</section>

			</footer>
			<!-- /footer -->

		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>

		<!-- analytics -->
		<!-- <script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
		ga('send', 'pageview');
		</script> -->

	</body>
</html>
