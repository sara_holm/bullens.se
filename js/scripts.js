(function ($, root, undefined) {

	$(function () {

		'use strict';

		// DOM ready, take it away

		$('#mobile-menu').click(function() {
			if($('#mobile-menu').hasClass('menu-active')) {
				$('#mobile-menu').removeClass('menu-active');
				$('.menu').css('opacity', '0');
				// TODO: set timeout display none
				setTimeout(function() {
					$('.menu').css('display', 'none');
				}, 300);
			} else {
				$('#mobile-menu').addClass('menu-active');
				$('.menu').css('display', 'block');
				setTimeout(function() {
					$('.menu').css('opacity', '1');
				}, 50);
			}

		});

		// Start page slider
		var current;
		var totalSlides = $('.topSlider').children().length;
		console.log(totalSlides);

		if($(window).width() < 600 && totalSlides > 1) {
			setInterval(function() {
				next();
			}, 5000);
		}
		if(totalSlides == 1) {
		  $('.slideSelector').css('display', 'none');
		}
		var container = $('<div class="numDiv" />');

		for(var i = 0; i < totalSlides; i++) {
			var id = i + 1;
			if(id == 1) {
				container.append('<span class="sliderNum active" id="slideNum'+ id + '">' + id + '</span>');
			} else {
			  container.append('<span class="sliderNum" id="slideNum'+ id + '">' + id + '</span>');
			}
		}

		$('.numContainer').html(container);

		$('.sliderNum').on('click touch', function(e) {

			var clickedNum = e.target.id;
			console.log(clickedNum);
			var lastChar = parseInt(clickedNum.slice(-1));
			current = parseInt($('.activeSlide').attr('id'));
			if(lastChar !== current) {
				var lastCharId = '#' + lastChar;
				var currentId = '#' + current;
				var clickedNumId = '#' + clickedNum;
				var notActive = '#slideNum' + current;

				$(lastCharId).removeClass('inactiveSlide');
				$(currentId).removeClass('activeSlide');
				$(notActive).removeClass('active');

				$(currentId).addClass('inactiveSlide');
				$(lastCharId).addClass('activeSlide');
				$(clickedNumId).addClass('active');
			}
		});


		$('.prev').on('click touch', function() {
			prev();
		})
		$('.next').on('click touch', function() {
			next();
		});

		//Previous slide
		function prev() {
			current = parseInt($('.activeSlide').attr('id'));
			var prev;

			if(current == 1) {
			  prev = totalSlides;
			} else {
				prev = current - 1;
			}

			var prevId = '#' + prev;
			var currentId = '#' + current;
			var notActive = '#slideNum' + current;
			var clickedNumId = '#slideNum' + prev;

			$(prevId).removeClass('inactiveSlide');
			$(currentId).removeClass('activeSlide');
			$(notActive).removeClass('active');

			$(currentId).addClass('inactiveSlide');
			$(prevId).addClass('activeSlide');
			$(clickedNumId).addClass('active');
			console.log(clickedNumId)
		}
		//Next slide
		function next() {
			current = parseInt($('.activeSlide').attr('id'));
			var next;

			if(current == totalSlides) {
				next = 1;
			} else {
				next = current + 1;
			}

			var nextId = '#' + next;
			var currentId = '#' + current;
			var notActive = '#slideNum' + current;
			var clickedNumId = '#slideNum' + next;

			$(nextId).removeClass('inactiveSlide');
			$(currentId).removeClass('activeSlide');
			$(notActive).removeClass('active');

			$(currentId).addClass('inactiveSlide');
			$(nextId).addClass('activeSlide');
			$(clickedNumId).addClass('active');
		}

		//Masonry.js
		var elem = document.querySelector('.grid');
		var msnry = new Masonry( elem, {
		// options
		itemSelector: '.grid-item',
		columnWidth: '.span_6_of_12',
		percentPosition: true
		});
	});

})(jQuery, this);
